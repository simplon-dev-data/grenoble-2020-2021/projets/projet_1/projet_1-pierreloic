#!/bin/bash

# Constants
DB_NAME=$1

# Run sql script for cleaning
sqlite3 $DB_NAME <<EOS
.read "clean_data.sql"
EOS
