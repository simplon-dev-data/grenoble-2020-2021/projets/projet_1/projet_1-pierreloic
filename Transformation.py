#!/usr/bin/env python
# coding: utf-8

# # Note de transformation des données

# In[1]:


import csv
import re
import logging
import ipaddress
import user_agents


# In[7]:


FILE_PATH = r"/home/pl/Simplon/projet_1/unzip_data/access.log"
FILE_CSV = r"data.csv"
IP_FILE = r"IPV4.csv"
NB_LIGNES = 5


# In[8]:


logging.basicConfig(filename='pipeline_logs.log', level=logging.DEBUG)


# In[9]:


def ip_loc(ip_add):
    """ Find the latitude and longitude for a ip V4 address"""
    with open(IP_FILE) as file:
        reader = csv.DictReader(file)
        for row in reader:
            try:
                if ipaddress.ip_address(ip_add) in ipaddress.ip_network(row["network"]):
                    return row["latitude"], row["longitude"]
            except ValueError:
                logging.warning(f"Erreur d'adresse Ip : {ip_add}")
                return "", ""
    return "", ""


# In[10]:


def extract(rawline):
    """ Extract log data from file in a dictionnary """
    regex = r"""^(?P<ip>(?:\d{1,3}.){3}\d{1,3}) (?P<user_id_client>[^ ]+) (?P<user_id_person>[^ ]+) \[(?P<date>[^\] :]+):(?P<time>[^\] ]+) (?P<timezone>[^\] ]+)\] \"(?P<method>[^ ]+) (?P<url>[^ ]+) (?P<protocol>[^ \"]+)\" (?P<status_code>\d+) (?P<size>\d+) \"(?P<referrer>[^\"]+)\" \"(?P<user_agent>[^\"]+)\" \"(?P<end>[^\"]+)\"$"""
    result = re.search(regex, rawline)
    if result is not None:
        dico = result.groupdict()
        # Regex for the date
        regex_date = r"""^(?P<day>\d{2})/(?P<month>[A-Za-z]{3})/(?P<year>\d{4})"""
        result_date = re.search(regex_date, dico["date"])
        dict_month = {
            "Jan" : "01",
            "Feb" : "02",
            "Mar" : "03",
            "Apr" : "04",
            "May" : "05",
            "Jun" : "06",
            "Jul" : "07",
            "Aug" : "08",
            "Sep" : "09",
            "Oct" : "10",
            "Nov" : "11",
            "Dec" : "12",
        }
        dico_date = result_date.groupdict()
        dico["date"] = dico_date["year"] + "-" + dict_month[dico_date["month"]] + "-" + dico_date["day"]
        dico["date"] = dico["date"] + " " + dico["time"]
        # User agent
        user_agent = user_agents.parse(dico["user_agent"])
        dico["browser_family"] = user_agent.browser.family
        dico["browser_version"] = user_agent.browser.version_string
        dico["os_family"] = user_agent.os.family
        dico["os_version"] = user_agent.os.version_string
        dico["device_family"] = user_agent.device.family
        dico["device_brand"] = user_agent.device.brand
        dico["device_model"] = user_agent.device.model
        # Latitude / longitude
        dico["latitude"], dico["longitude"] = ip_loc(dico["ip"])
        return dico


# In[11]:


nombre_logs = 0
nombre_erreurs = 0
with open(FILE_CSV, 'w', newline='') as csvfile:
    with open(FILE_PATH) as file:
        while True:
            if nombre_logs == 0:
                line = file.readline()
                fieldnames = list(extract(line).keys())
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                writer.writeheader()
            else:
                line = file.readline()
            if not line:
                break
            extraction = extract(line)
            if extraction is not None:
                writer.writerow(extract(line))
            else:
                nombre_erreurs += 1
                warning_message = f"""Erreur N°{nombre_erreurs}\nErreur à la ligne {nombre_logs}\nContenu de la ligne en erreur :\n{line}\nGroupes détectés :\n{str(extract(line))}"""
                logging.warning(warning_message)
            nombre_logs += 1
            if nombre_logs == NB_LIGNES:
                break
print(f"Il y a {nombre_logs} logs dans le fichier")
print(f"Il y a eu {nombre_erreurs} erreurs lors de la création du fichier")
print(f"Le fichier de logs s'appelle {FILE_CSV}")


# In[ ]:




