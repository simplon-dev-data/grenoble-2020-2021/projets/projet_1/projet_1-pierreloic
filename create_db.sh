#!/bin/bash

# Run ./create_db.sh <csv file name> <sqlite database name>

# Constants
FILE_NAME=$1
DB_NAME=$2

# Create SQLite DB from CSV file
sqlite3 $DB_NAME <<EOS
.mode csv
.separator ,
.import $FILE_NAME raw_data
EOS
