


URL="https://dataverse.harvard.edu/api/access/datafile/:persistentId?persistentId=doi:10.7910/DVN/3QBYB5/NXKB6J"
FILE_NAME="log_data.zip"

echo "Début téléchargement du fichier de logs de la base d'Harvard"
wget -qO $FILE_NAME $URL
echo "Fin du téléchargement du fichier $FILE_NAME"
