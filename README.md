# Projet_1-PierreLoic

Ce repository contient les différents codes que j'ai mis en place pour tester le projet 1.

Le pipeline de données est écrit en bash, il est dans le fichier `main.sh`. Il lance différents scripts bash et python.

Les étapes du pipeline sont les suivantes :

1 - Téléchargement

2 - Décompression

3 - Création du fichier CSV

4 - Création de la base de données SQLite

5 - Nettoyage des données

6 - Envoi de la base de données vers Metabase
