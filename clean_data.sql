CREATE TABLE clean_data AS
   SELECT ip, date, url, referrer, browser_family, os_family, device_family, latitude, longitude FROM raw_data;
