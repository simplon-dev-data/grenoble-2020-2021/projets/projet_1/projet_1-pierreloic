#!/bin/bash

# Define constants
FILE_NAME=$1
FOLDER_PATH=$(pwd)

echo "Le fichier $FILE_NAME est en cours de décompression"

# Unzip
echo "La décompression s'effectue à cette adresse $FOLDER_PATH/unzip_data" 
unzip -q $FILE_NAME -d "$FOLDER_PATH/unzip_data"
