echo "--------ETAPE 1 : TELECHARGEMENT -------"

FILE_NAME="log_data.zip"
if test -s $FILE_NAME ; then
    echo "$FILE_NAME existe déjà. Pas de nouveau téléchargement"
else 
    echo "$FILE_NAME n'existe pas. Téléchargement..."
    ./download.sh
fi

echo "-------ETAPE 2 : DECOMPRESSION --------"
if test -d "unzip_data" ; then
    # rm -r unzip_data 
    echo "Le fichier de données a déjà été décompressé"
else
    ./dezip.sh $FILE_NAME
fi

echo "-------ETAPE 3 : CREATION DU FICHIER CSV ---------"
if test -d "data.csv" ; then
    sudo rm "data.csv" 
fi
echo "Création du fichier CSV"
python3 -m pip install pyyaml ua-parser user-agents
python3 "Transformation.py"

echo "-------ETAPE 4 : CREATION DE LA BASE DE DONNEES SQLITE -----"
if test -d "data.db" ; then
    sudo rm "data.db" 
fi

echo "Création de la base de données SQLite..."
./create_db.sh "data.csv" "data.db"
echo "Base de données créée"

echo "------ETAPE 5 : NETTOYAGE DES DONNEES-----"
./clean_db.sh "data.db"
echo "Base de données nettoyée"

echo "------ETAPE 6 : ENVOI VERS METABASE-------"
echo "Copie de la base de données vers le conteneur Docker de Metabase..."
cp data.db .docker/metabase/data.db
echo "Copie effectuée" 
